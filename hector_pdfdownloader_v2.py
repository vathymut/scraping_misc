# -*- coding: utf-8 -*-
"""
Created on Thu Aug 08 22:12:39 2013

@author: Vathy M. Kamulete
"""
import urllib2
import shutil
import urlparse
import os
import datetime

# Assume that the url provided already points to the .pdf file
# url = 'http://www.kevinsheppard.com/images/0/09/Python_introduction.pdf'

def give_filename( url ):
    '''
    Give the pdf a name by extracting final component of pathname.
    '''
    netloc, path = urlparse.urlsplit( url )[ 1:3 ]
    filename = os.path.basename( path )
    now_datetime = datetime.datetime.now( )
    now_string = now_datetime.strftime( "%Y-%m-%d-%H-%M-%S" )
    if filename[-3:] == 'pdf':
        filename = filename[:-4] + '-' + now_string + '.pdf'
        return filename
    else:
        list_truncated_domain = netloc.split('.')
        domain_name = list_truncated_domain[ 1 ]
        filename = domain_name + '-' + now_string + '.pdf'
        return filename
            

def download( url, filename = None ):
    '''
    Download and name the pdf file
    '''
    r = urllib2.urlopen( urllib2.Request(url) )
    try:
        if filename is None:
            filename = give_filename( url )
        with open(filename, 'wb') as f:
            shutil.copyfileobj( r, f )
    finally:
        r.close()

# Hector's example
# for url in urls:
#   download( url )