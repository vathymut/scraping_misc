# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 03:13:08 2013

@author: Vathy
"""

from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.spiders import CrawlSpider
# from scrapy.http import FormRequest, Request

from iiroc.items import iirocItem

from unidecode import unidecode

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

import time

# For testing purposes
LIST_OF_FIRMS_TO_TEST = ['Altamira Securities', 
                         'Bieber Securities Inc.',
                         'Brault, Guy, O\'Brien Inc.', 
                         'First Delta Securities Inc.',
                         'Placements Banque Nationale inc.' ]                       
             
# Paramaters for the query
SEARCHURL = "http://www.securities-administrators.ca/nrs/nrsearch.aspx?id=850"
FIRMNAME_INPUT_ID = "ctl00_bodyContent_txtFirmName"
ACTIVE_BOX_ID = "ctl00_bodyContent_chkActive"
HIST_BOX_ID = "ctl00_bodyContent_chkHistPrior"
FIRM_BOX_ID = "ctl00_bodyContent_rdFirm"
SEARCH_ID = "ctl00_bodyContent_ibtnSearch"
# TABLE_ID = "ctl00_bodyContent_gv_frm_list"
# FIRST_LINK_ID = "ctl00_bodyContent_gv_frm_list_ctl02_lbtnFirmDetail"
HIST_INFO_ID = "ctl00_bodyContent_lbtnShowFirmHistorical"
# FIRM_NAME_ID = 'ctl00_bodyContent_lbl_firm_name'