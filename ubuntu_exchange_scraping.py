# from http://thepcspy.com/read/scraping-websites-with-python/

import urllib2
from BeautifulSoup import BeautifulSoup

url = 'http://ubuntu.stackexchange.com/questions?sort=newest'
source = urllib2.urlopen(url)

soup = BeautifulSoup(source)
h3s = soup.findAll('h3')

for h3 in h3s[:3]:
    # print h3.contents[0]
    title = h3.contents[0].string
    link = h3.contents[0]['href']
    print '%s\n%s\n' % (title, link)
    
