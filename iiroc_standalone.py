# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 03:13:08 2013

@author: Vathy
"""

#from scrapy.selector import HtmlXPathSelector
#from scrapy.contrib.spiders import CrawlSpider
#from scrapy.http import FormRequest, Request, Response, TextResponse
from unidecode import unidecode
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
# stripped down BoltBus script 
#from selenium.common.exceptions import TimeoutException
#from selenium.webdriver.support.ui import WebDriverWait 
#from selenium.webdriver.support import expected_conditions as EC
import time

# For testing purposes
LIST_OF_FIRMS_TO_TEST = ['Altamira Securities', 
                         'Bieber Securities Inc.',
                         'Brault, Guy, O\'Brien Inc.', 
                         'First Delta Securities Inc.',
                         'Placements Banque Nationale inc.' ]                       
             
# Paramaters for the query
SEARCHURL = "http://www.securities-administrators.ca/nrs/nrsearch.aspx?id=850"
FIRMNAME_INPUT_ID = "ctl00_bodyContent_txtFirmName"
ACTIVE_BOX_ID = "ctl00_bodyContent_chkActive"
HIST_BOX_ID = "ctl00_bodyContent_chkHistPrior"
FIRM_BOX_ID = "ctl00_bodyContent_rdFirm"
SEARCH_ID = "ctl00_bodyContent_ibtnSearch"
HIST_INFO_ID = "ctl00_bodyContent_lbtnShowFirmHistorical"
# FIRM_NAME_ID = 'ctl00_bodyContent_lbl_firm_name'
# TABLE_ID = "ctl00_bodyContent_gv_frm_list"
# FIRST_LINK_ID = "ctl00_bodyContent_gv_frm_list_ctl02_lbtnFirmDetail"      

for firm_name in LIST_OF_FIRMS_TO_TEST:
    # Create a new instance of the Firefox driver
    driver = webdriver.Firefox()
    driver.get( SEARCHURL )   
    
    # Wait for javascript to load in Selenium
    time.sleep(4)
    
    # Get the historical data for the firm  
    # Add firm name in search field
    firmnamefield = driver.find_element_by_id( FIRMNAME_INPUT_ID )
    firmnamefield.send_keys( firm_name )
    
    # Uncheck active box
    activeboxcheck = driver.find_element_by_id( ACTIVE_BOX_ID )
    activeboxcheck.click( )
    time.sleep(2.5)
    
    # Check Historical prior to September 28, 2009 box
    histboxcheck = driver.find_element_by_id( HIST_BOX_ID )
    histboxcheck.click( )
    time.sleep(2.5)
    
    # Check search by firm name box
    firmboxcheck = driver.find_element_by_id( FIRM_BOX_ID )
    firmboxcheck.click( )
    time.sleep(2.5)
    
    # Click on search 
    search = driver.find_element_by_id( SEARCH_ID )
    search.click( )
    time.sleep(2.5)
    
    # Extract the first link from the table of query results
    firstlink = driver.find_element_by_xpath("//table[@id = 'ctl00_bodyContent_gv_frm_list' ]/tbody/tr/td/a[1]")
    firstlink.click( )
    time.sleep(2.5)
    
    # Extract the firm historical information link
    histinfo = driver.find_element_by_id( HIST_INFO_ID )
    histinfo.click( )
    time.sleep(2.5)
    
    # Extract info
#    hxs = HtmlXPathSelector(response)
#    item = iirocItem()
#    item[ 'firm_name' ] = firm_name
#    # Get alternative firm name if any
#    alt_names = hxs.select("//@id = 'ctl00_bodyContent_lbl_firm_name'/text()").extract()
#    alt_names = [ unidecode(name) for name in alt_names ]
#    item[ 'alt_names' ] = firm_name
#    print item
#    yield item
 