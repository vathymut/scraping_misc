# -*- coding: utf-8 -*-
"""
Created on Thu May 30 20:24:45 2013

@author: Vathy
"""

import schedule
import time

def job():
    print("I'm working...")

schedule.every(1).minutes.do(job)

while 1:
    schedule.run_pending()
    time.sleep(1)
    
