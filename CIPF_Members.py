########################################################
# Find CIPF Members
# IIROC Project for Jason
########################################################

from BeautifulSoup import BeautifulSoup
from urllib2 import urlopen
import csv
import re
import time

# Find CIPF Members
UrlLink = "http://www.cipf.ca/public/MemberDirectory/PastMembers.aspx"
RawData = urlopen( UrlLink ).read()
CIPFSoup = BeautifulSoup( RawData ) # Can be parsed with lxml as BeautifulSoup( RawData, "lxml" ) in bs4
TablePastMembers = CIPFSoup.find( "tbody" ) # Get the table
TableRows = TablePastMembers.findAll( 'tr' ) # Get All rows of table

# Create a generator for the valid rows of the table
def good_rows(table_of_rows):
	for row in table_of_rows:
		# Check if id key exists
		try:
			if row['id']:
				yield row
		except KeyError:
			continue


with open('past_cipf_members.csv', 'wb') as f:
	for row in good_rows( TableRows ): 
		# Get columns of the row
		cols = row.findAll( 'td' ) 
		# Name of the company
		name = cols[1].contents[0].strip().encode('utf-8')
		# Date of entry
		# entrydate = cols[2].contents[0].strip().encode('utf-8')
		# Date of exit 
		exitdate = cols[3].contents[0].strip().encode('utf-8')
		print name, exitdate #, entrydate
		# Write to csv file
		writer = csv.writer(f)
		writer.writerow( [ name, exitdate ] )

print '\nAuthor: Vathy M. Kamulete \nDate Last Modified: 04/23/2013' 
print 'Suffer for your craft\n'