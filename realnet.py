########################################################
# Explore for now
# RealNet Project for Jason
########################################################

from BeautifulSoup import BeautifulSoup
import requests

# Log in
UrlLoginPage = "https://realinfo.realnet.ca/ri/login.do?exec=login"

# Here is the data that FireBug said we sent
PostDict = {'passwd' : 'Mdhy78et', 
            'terms' : 'agree', 
            'username' : 'allj@bankofcanada.ca' }
           
HeadersDict = {'User-Agent': \
'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0'}

# Open persistent session first
s = requests.Session( )

# Send request
PostResponse = s.post(UrlLoginPage, 
                      allow_redirects = True, 
                      headers = HeadersDict,
                      data = PostDict)

# See if logged in
print 'You are now logged in to RealNet' in PostResponse.content
                          
# Parse it with BeautifulSoup
# Can be parsed with lxml as BeautifulSoup( RawData, "lxml" ) in bs4
RealNetSoup = BeautifulSoup( PostResponse.content ) 